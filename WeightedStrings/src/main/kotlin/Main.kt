fun main(args: Array<String>) {

    // input 1
    val s = "abbcccd"
    val queries = listOf(1, 3, 9, 8)
    val result = weightedStrings(s, queries)
    println(result)

    // input 2
    val s2 = "efffggh"
    val queries2 = listOf(4, 5, 6, 8, 14)
    val result2 = weightedStrings(s2, queries2)
    println(result2)

    // input 3
    val s3 = "ijjkklll"
    val queries3 = listOf(11, 12, 9, 7, 20, 36)
    val result3 = weightedStrings(s3, queries3)
    println(result3)

}

fun getWeight(c: Char): Int {
    return c - 'a' + 1
}

fun getSubstringWeights(s: String): Set<Int> {

    val weights = mutableSetOf<Int>()

    var i = 0

    while (i < s.length) {
        var count = 1
        while (i + count < s.length && s[i] == s[i + count]) {
            count++
        }

        val charWeight = getWeight(s[i])
        for (j in 1..count) {
            weights.add(charWeight * j)
        }
        i += count
    }

    return weights
}

fun weightedStrings(s: String, queries: List<Int>): List<String> {
    val weights = getSubstringWeights(s)
    return queries.map { if (it in weights) "Yes" else "No" }
}