fun main(args: Array<String>) {

    val sample1 = "3943"
    val k1 = 1
    println(highestPalindrome(sample1, k1))

    val sample2 = "932239"
    val k2 = 2
    println(highestPalindrome(sample2, k2))

    val sample3 = "24632422"
    val k3 = 3
    println(highestPalindrome(sample3, k3))

}

fun highestPalindrome(s: String, k: Int): String {
    val n = s.length
    val chars = s.toCharArray()

   fun makePalindrome(left: Int, right: Int, k: Int): Boolean {

       if (left >= right) return k >= 0

       if (chars[left] == chars[right]) {
           return makePalindrome(left + 1, right - 1, k)
       } else if (k > 0) {
           val maxChar = maxOf(chars[left], chars[right])
           chars[left] = maxChar
           chars[right] = maxChar
           return makePalindrome(left + 1, right - 1, k -1)
       }

       return false
   }

    if (!makePalindrome(0, n - 1, k)) return "-1"

    var remainingK = k
    for (i in 0 until n / 2) {
        if (chars[i] != '9') {
            if (remainingK >= 2 && chars[i] == s[i] && chars[n - 1 - i] == s[n - 1 - i]) {
                chars[i] = '9'
                chars[n - 1 - i] = '9'
                remainingK -= 2
            } else if (remainingK >= 1 && (chars[i] != s[i] && chars[n - 1 - i] != s[n - 1 - i])) {
                chars[i] = '9'
                chars[n - 1 - i] = '9'
                remainingK -= 1
            }
        }
    }

    if (n % 2 != 0 && remainingK > 0) {
        chars[n/2] = '9'
    }

    return String(chars)
}