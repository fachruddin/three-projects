fun main(args: Array<String>) {
    // samples 1
    val samples = listOf(
        "{ [ ( ) ] }",
        "{ [ ( ] ) }",
        "{ ( ( [ ] ) [ ] ) [ ] }"
    )

    for (sample in samples) {
        println("Input: $sample")
        println("Output: ${isBalanced(sample)}")
    }

    // samples 2
    val samples2 = listOf(
        "{ [ ( ) ( ) ] }",
        "{ [ ( ] [ ) ) }",
        "{ ( ( [ ] ) [ ] ( ) ) [ ] }"
    )

    for (sample2 in samples2) {
        println("Input: $sample2")
        println("Output: ${isBalanced(sample2)}")
    }

    // samples3
    val samples3 = listOf(
        "{ [ ( ) ] [ ] [ ] ( ) }",
        "{ { ( ) [ ] ( ) } }",
        "{ ( [ ] [ ] ) [ ] ( [ ] { } ) }"
    )

    for (sample3 in samples3) {
        println("Input: $sample3")
        println("Output: ${isBalanced(sample3)}")
    }
}

fun isBalanced(s: String): String {
    val stack = mutableListOf<Char>()

    val opening = setOf('(', '{', '[')
    val matching = mapOf(
        ')' to '(',
        '}' to '{',
        ']' to '['
        )

    for (char in s) {

        when {
            char in opening -> stack.add(char)
            char in matching.keys -> {
                if (stack.isEmpty() ||
                    stack.removeAt(stack.size - 1) != matching[char]) {
                    return "NO"
                }
            }
        }
    }

    return if (stack.isEmpty()) "YES" else "NO"
}
