## Three Project
Repositori ini berisi 3 proyek,
1. Weighted Strings
2. Balanced Bracket
3. Highest Palindrome

## Deskripsi
Ketiga proyek tersebut ditulis dalam Bahasa Kotlin.<br>
Kode file utama ada di dalam nama proyek (Weighted Strings, dst).<br>
  Nama proyek > src > main > kotlin.<br>
Screenshot output ada di dalam folder Screenshot.

## Detail Kompleksitas Project Balanced Bracket
Untuk menganalisis kompleksitas dari fungsi `isBalanced` yang ada di dalam project, berikut langkah - langkahnya:

### Analisis Kompleksitas:

1. **Inisialisasi:**
   - `val stack = mutableListOf<Char>()` menginisialisasi sebuah daftar kosong.
   - `val opening = setOf('(', '{', '[')` menginisialisasi sebuah set dengan 3 elemen.
   - `val matching = mapOf(')' to '(', '}' to '{', ']' to '[')` menginisialisasi sebuah map dengan 3 pasangan kunci-nilai.

   Inisialisasi ini membutuhkan waktu konstan, \( O(1) \).

2. **Iterasi atas string:**
   ```kotlin
   for (char in s) {
       when {
           char in opening -> stack.add(char)
           char in matching.keys -> {
               if (stack.isEmpty() || stack.removeAt(stack.size - 1) != matching[char]) {
                   return "NO"
               }
           }
       }
   }
   ```
   - Loop ini mengiterasi setiap karakter dalam string input `s`. Jika panjang `s` adalah `n`, loop ini berjalan sebanyak \( O(n) \) kali.

3. **Operasi Stack:**
   - `stack.add(char)`: Menambahkan elemen ke akhir daftar (digunakan sebagai stack) membutuhkan waktu \( O(1) \).
   - `stack.isEmpty()`: Mengecek apakah daftar kosong membutuhkan waktu \( O(1) \).
   - `stack.removeAt(stack.size - 1)`: Menghapus elemen terakhir dari daftar membutuhkan waktu \( O(1) \).
   - Kondisi `char in opening` dan `char in matching.keys` masing-masing membutuhkan waktu \( O(1) \) karena ini adalah pencarian dalam set dan map.

4. **Pengecekan Akhir Stack:**
   ```kotlin
   return if (stack.isEmpty()) "YES" else "NO"
   ```
   Pengecekan akhir ini membutuhkan waktu \( O(1) \).

### Kompleksitas Detail:

- Loop mengiterasi setiap karakter dalam string sekali, membuat operasi utama linear sehubungan dengan panjang string \( n \). Dalam setiap iterasi:
  - Mengecek apakah karakter adalah tanda kurung pembuka membutuhkan waktu \( O(1) \).
  - Menambahkan karakter ke stack membutuhkan waktu \( O(1) \).
  - Mengecek apakah karakter adalah tanda kurung penutup membutuhkan waktu \( O(1) \).
  - Mengecek apakah stack kosong dan menghapus elemen terakhir dari stack masing-masing membutuhkan waktu \( O(1) \).
  - Memetakan tanda kurung penutup ke tanda kurung pembuka yang sesuai dan membandingkan karakter membutuhkan waktu \( O(1) \).

Jadi, kompleksitas waktu keseluruhan dari fungsi `isBalanced` adalah:

\[ O(n) \]

### Kompleksitas Ruang:

- Kompleksitas ruang ditentukan oleh ruang yang digunakan untuk stack.
- Dalam kasus terburuk, jika semua karakter dalam string adalah tanda kurung pembuka, stack akan menyimpan semua \( n \) karakter.

Jadi, kompleksitas ruang dari fungsi `isBalanced` adalah:

\[ O(n) \]

### Ringkasan:

- **Kompleksitas Waktu:** \( O(n) \), di mana \( n \) adalah panjang string input.
- **Kompleksitas Ruang:** \( O(n) \), karena penggunaan stack dalam kasus terburuk.

Analisis kompleksitas ini menunjukkan bahwa fungsi ini secara efisien memeriksa keseimbangan tanda kurung dalam waktu linear, membuatnya cocok untuk string yang panjang.
